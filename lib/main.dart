import 'package:flutter/material.dart';
import 'package:log/pages/board_screen.dart';
import 'package:log/pages/register_screen.dart';
import 'package:log/pages/registration_screen.dart';
import 'package:log/pages/task_screen.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => const RegisterScreen(),
        '/board_screen': (context) => const BoardScreen(),
        '/task': (context) => const CreateTaskScreen(),
        '/registration': (context) => const RegistrationScreen(),
      },
    );
  }
}
