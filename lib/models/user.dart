class User {
  final int id;
  final String name;
  final String surname;
  final String patronomyc;
  final String login;
  final String email;

  User(this.id, this.name, this.surname, this.patronomyc, this.login,
      this.email);

  factory User.fromJson(Map<String, dynamic> json) => User(
      json['user_id'],
      json['surname'],
      json['user_name'],
      json['patronymic'],
      json['login'],
      json['email']);
}
