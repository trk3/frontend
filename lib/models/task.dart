import 'package:log/models/user.dart';

class Task {
  final int id;
  final String name;
  final User author;
  final User executor;
  final String date;
  final int status;
  final String taskDescription;

  Task(this.id, this.name, this.author, this.executor, this.date, this.status,
      this.taskDescription);

  factory Task.fromJson(Map<String, dynamic> json) => Task(
        json['task_id'],
        json['task_name'],
        User.fromJson(json['author']),
        User.fromJson(json['executor']),
        json['task_create_date'],
        json['status'],
        json['task_description'],
      );

  Task changeStatus(int newStatus) {
    return Task(
        id, name, author, executor, date, newStatus,
        taskDescription
    );
  }

  Task editTask({String? newName, String? newDescription}) {
    return Task(id, newName ?? name, author, executor, date, status,
        newDescription ?? taskDescription);
  }
}
