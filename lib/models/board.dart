import 'package:log/models/task.dart';

class Board {
  final String id;
  final String name;
  final List<Task> tasks;

  Board(this.id, this.name, this.tasks,);

  Board addNewTask(Task task) {
    List<Task> newTasks = tasks;
    newTasks.add(task);
    return Board(id, name, newTasks);
  }

  Board changeTaskStatus(Task task, int newStatus) {
    List<Task> newTasks = tasks;
    tasks.remove(task);
    tasks.add(task.changeStatus(newStatus));
    return Board(id, name, newTasks);
  }
}