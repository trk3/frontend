import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:log/constants.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void dispose() {
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: [
          const Expanded(
            flex: 3, // 20%
            child: SizedBox(),
          ),
          Expanded(
            flex: 6, // 60%
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Expanded(child: Image(image: AssetImage('logo.png'))),
                TextField(
                  controller: usernameController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Введите логин',
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextField(
                  controller: passwordController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Введите пароль',
                  ),
                  obscureText: true,
                ),
                const SizedBox(
                  height: 30,
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: OutlinedButton(
                        onPressed: () async {
                          var url = Uri.parse(
                              "http://127.0.0.1:8000/api-token-auth/");
                          var body = {
                            "username": usernameController.text,
                            "password": passwordController.text
                          };

                          var response = await http.post(url,
                              body: json.encode(body),
                              headers: {'Content-Type': 'application/json'});
                          if (jsonDecode(response.body).containsKey('token')) {
                            loginName = usernameController.text;
                            Navigator.pushNamed(context, '/board_screen');
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                content:
                                    Text('Неправильный логин или пароль')));
                          }
                          debugPrint(response.body);
                        },
                        child: const Text('Войти'),
                      ),
                    ),
                    const Expanded(
                      flex: 2,
                      child: SizedBox(),
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        children: [
                          const Text('Еще не зарегестрированы?'),
                          OutlinedButton(
                            style: OutlinedButton.styleFrom(
                              foregroundColor: Colors.amberAccent,
                              backgroundColor: Colors.blueGrey,
                            ),
                            onPressed: () async {
                              Navigator.pushNamed(context, '/registration');
                            },
                            child: const Text('Зарегистрироваться'),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                const SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
          const Expanded(
            flex: 3, // 20%
            child: SizedBox(),
          )
        ],
      ),
    );
  }
}
