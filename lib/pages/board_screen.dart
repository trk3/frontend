import 'dart:convert';

import 'package:boardview/board_item.dart';
import 'package:boardview/board_list.dart';
import 'package:boardview/boardview.dart';
import 'package:boardview/boardview_controller.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:log/constants.dart';
import 'package:log/models/board.dart';
import 'package:log/models/task.dart';
import 'package:log/pages/watch_task.dart';

class BoardScreen extends StatefulWidget {
  const BoardScreen({Key? key}) : super(key: key);

  @override
  State<BoardScreen> createState() => _BoardScreenState();
}

class _BoardScreenState extends State<BoardScreen> {
  BoardViewController boardViewController = BoardViewController();
  bool loading = true;
  late Board chosenBoard;

  void getData() async {
    if (!loading) return;
    Set<Board> setBoards = {};
    Response resp = await get(
        Uri.parse('http://127.0.0.1:8000/tasks/getboards/?name=nikitinnikola'));
    Map<String, dynamic> data = jsonDecode(resp.body);

    for (String id in data.keys.toList()) {
      List<Task> tasks = [];
      Map<String, dynamic> board = data[id];
      for (String taskId in board['board_tasks'].keys.toList()) {
        tasks.add(Task.fromJson(board['board_tasks'][taskId]));
      }
      setBoards.add(Board(id, board['board_name'], tasks));
    }
    boards = setBoards.toList();
    chosenBoard = boards[0];
    for (Board board in boards) {
      boardNames.add(board.name);
    }
    setState(() {
      loading = false;
    });
  }

  List<BoardList> lists = [];

  List<BoardItem> createItemList(String type) {
    List<BoardItem> items = [];
    for (Task task in chosenBoard.tasks) {
      if (taskTypes[task.status] == type) {
        items.add(
          BoardItem(
              onDropItem: (int? listIndex, int? itemIndex, int? oldListIndex,
                  int? oldItemIndex, BoardItemState? state) async {
                // print(oldListIndex);
                // print(listIndex);
                // print(oldItemIndex);
                // print(itemIndex);

                Task task = taskLists[oldListIndex!][oldItemIndex!];
                taskLists[oldListIndex].removeAt(oldItemIndex);
                task.changeStatus(listIndex!);
                taskLists[listIndex]
                    .insert(itemIndex!, task.changeStatus(listIndex));

                var payload = {
                  "get_post": {"task_id": task.id},
                  "change_post": {
                    "executor_login": loginName,
                    "board_name": chosenBoard.name,
                    "task_name": task.name,
                    "status": listIndex,
                    "task_description": task.taskDescription
                  }
                };
                print(payload);
                Response resp = await post(
                    Uri.parse('http://127.0.0.1:8000/tasks/edittask/'),
                    body: jsonEncode(payload));
                print(resp.body);
              },
              onTapItem: (int? listIndex, int? itemIndex,
                  BoardItemState? state) async {
                showTask(context, listIndex!, itemIndex!, chosenBoard.name);
              },
              item: Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(task.name),
                ),
              )),
        );
      }
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    getData();
    double width = MediaQuery.of(context).size.width;
    if (!loading) {
      Set<BoardList> setBoards = {};
      Set<List<Task>> setListTask = {};
      for (String type in taskTypes.values.toList()) {
        Set<Task> setTasks = {};
        List<BoardItem> items = [];
        for (Task task in chosenBoard.tasks) {
          if (taskTypes[task.status] == type) {
            setTasks.add(task);
            items.add(
              BoardItem(
                  onDropItem: (int? listIndex, int? itemIndex, int? oldListIndex,
                      int? oldItemIndex, BoardItemState? state) async {

                    Task task = taskLists[oldListIndex!][oldItemIndex!];
                    taskLists[oldListIndex].removeAt(oldItemIndex);
                    task.changeStatus(listIndex!);
                    taskLists[listIndex]
                        .insert(itemIndex!, task.changeStatus(listIndex));

                    var payload = {
                      "get_post": {"task_id": task.id},
                      "change_post": {
                        "executor_login": loginName,
                        "board_name": chosenBoard.name,
                        "task_name": task.name,
                        "status": listIndex,
                        "task_description": task.taskDescription
                      }
                    };
                    print(payload);
                    Response resp = await post(
                        Uri.parse('http://127.0.0.1:8000/tasks/edittask/'),
                        body: jsonEncode(payload));
                    print(resp.body);
                  },
                  onTapItem: (int? listIndex, int? itemIndex,
                      BoardItemState? state) async {
                    showTask(context, listIndex!, itemIndex!, chosenBoard.name);
                  },
                  item: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(task.name),
                    ),
                  )),
            );
          }
        }
        setListTask.add(setTasks.toList());
        setBoards.add(BoardList(
          draggable: false,
          header: [
            Expanded(
                child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Text(
                      type,
                      style: const TextStyle(fontSize: 20),
                    ))),
          ],
          items: items,
        ));
      }
      taskLists = setListTask.toList();
      lists = setBoards.toList();
    }
    return Scaffold(
      body: loading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Column(
              children: [
                Expanded(
                  flex: 1,
                  child: DropdownButton(
                    style: const TextStyle(fontSize: 30),
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    icon: const Icon(Icons.bookmark_add_rounded),
                    focusColor: Colors.white,
                    dropdownColor: Colors.white,
                    items: boards
                        .map((e) => DropdownMenuItem(
                            value: e.name, child: Text(e.name)))
                        .toList(),
                    onChanged: (item) {
                      setState(() {
                        chosenBoard = boards
                            .firstWhere((element) => element.name == item);
                      });
                    },
                    value: chosenBoard.name,
                  ),
                ),
                Expanded(
                  flex: 10,
                  child: Center(
                    child: BoardView(
                      width: width / 6,
                      lists: lists,
                      boardViewController: boardViewController,
                    ),
                  ),
                ),
                Expanded(
                    child: Row(
                  children: [
                    const Expanded(child: SizedBox()),
                    // Expanded(
                    //   child: ElevatedButton(
                    //     onPressed: () {
                    //       setState(() {});
                    //     },
                    //     style: const ButtonStyle(
                    //         backgroundColor:
                    //             MaterialStatePropertyAll<Color>(Colors.blue)),
                    //     child: const Text('Обновить текущую доску'),
                    //   ),
                    // ),
                    // const Expanded(child: SizedBox()),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/task');
                        },
                        style: const ButtonStyle(
                            backgroundColor:
                                MaterialStatePropertyAll<Color>(Colors.blue)),
                        child: const Text('Создать новую задачу'),
                      ),
                    ),
                    const Expanded(child: SizedBox()),
                  ],
                ))
              ],
            ),
    );
  }
}
