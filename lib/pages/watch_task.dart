import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:log/constants.dart';
import 'package:log/models/task.dart';

void showTask(
    BuildContext context, int listIndex, int itemIndex, String boardName) {
  bool edit = false;
  Task task = taskLists[listIndex][itemIndex];

  TextEditingController headerController =
      TextEditingController(text: task.name);
  TextEditingController descriptionController =
      TextEditingController(text: task.taskDescription);

  showDialog(
      context: context,
      builder: (context) => StatefulBuilder(builder: (context, setState1) {
            return Dialog(
              insetPadding: const EdgeInsets.symmetric(horizontal: 200),
              child: SizedBox(
                width: 700,
                child: IntrinsicHeight(
                  child: Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Container(
                          margin: const EdgeInsets.only(left: 30),
                          color: Colors.white,
                          child: Column(
                            children: <Widget>[
                              const SizedBox(
                                height: 20,
                              ),
                              Row(
                                children: <Widget>[
                                  IconButton(
                                      onPressed: () {
                                        setState1(() {
                                          edit = !edit;
                                        });
                                      },
                                      icon: const Icon(Icons.edit)),
                                ],
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              edit
                                  ? TextField(
                                      controller: headerController,
                                      decoration:
                                          InputDecoration(border: border),
                                      onChanged: (text) {
                                        setState1(() {});
                                      },
                                    )
                                  : Text(
                                      task.name,
                                      style: const TextStyle(fontSize: 28),
                                    ),
                              const SizedBox(
                                height: 30,
                              ),
                              edit
                                  ? TextField(
                                      controller: descriptionController,
                                      onChanged: (text) {
                                        setState1(() {});
                                      },
                                      maxLines: 5,
                                      decoration:
                                          InputDecoration(border: border),
                                    )
                                  : Text(task.taskDescription),
                              const SizedBox(
                                height: 20,
                              ),
                              edit
                                  ? Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        ElevatedButton(
                                          onPressed: () async {
                                            var payload = {
                                              "get_post": {"task_id": task.id},
                                              "change_post": {
                                                "executor_login": loginName,
                                                "board_name": boardName,
                                                "task_name":
                                                    headerController.text,
                                                "status": task.status,
                                                "task_description":
                                                    descriptionController.text
                                              }
                                            };
                                            print(payload);
                                            Response resp = await post(
                                                Uri.parse(
                                                    'http://127.0.0.1:8000/tasks/edittask/'),
                                                body: json.encode(payload));
                                            taskLists[listIndex][itemIndex] = taskLists[listIndex][itemIndex]
                                                .editTask(
                                                    newName:
                                                        headerController.text,
                                                    newDescription:
                                                        descriptionController
                                                            .text);
                                            print(taskLists[listIndex][itemIndex].name);
                                            Navigator.pop(context);
                                          },
                                          style: const ButtonStyle(
                                              backgroundColor:
                                                  MaterialStatePropertyAll<
                                                      Color>(Colors.blue)),
                                          child:
                                              const Text('Применить изменения'),
                                        ),
                                        ElevatedButton(
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          style: const ButtonStyle(
                                              backgroundColor:
                                                  MaterialStatePropertyAll<
                                                      Color>(Colors.blue)),
                                          child: const Text('Отмена'),
                                        )
                                      ],
                                    )
                                  : const SizedBox.shrink(),
                              const SizedBox(
                                height: 30,
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Container(
                          // height: 310,
                          color: Colors.grey.shade200,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              rowWithTwoWords('Автор', task.author.login, false),
                              rowWithTwoWords('Исполнитель', task.executor.login, true),
                              rowWithTwoWords('Доска', boardName, false),
                              rowWithTwoWords(
                                  'Статус', taskTypes[task.status]!, false),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }));
}

Widget rowWithTwoWords(String first, String second, bool executor) {
  return Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
    const SizedBox(
      width: 20,
    ),
    Text(first),
    SizedBox(
      width: executor ? 55 : 100,
    ),
    Text(second),
  ]);
}
