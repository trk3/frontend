import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:log/constants.dart';
import 'package:log/models/task.dart';
import 'package:log/models/user.dart';
import 'package:log/pages/watch_task.dart';

class CreateTaskScreen extends StatefulWidget {
  const CreateTaskScreen({Key? key}) : super(key: key);

  @override
  State<CreateTaskScreen> createState() => _CreateTaskScreenState();
}

class _CreateTaskScreenState extends State<CreateTaskScreen> {
  TextEditingController headerController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  String deskValue = boardNames[0];
  String statusValue = 'Backlog';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: <Widget>[
          const Expanded(
            flex: 1,
            child: SizedBox(),
          ),
          Expanded(
            flex: 2,
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Container(
                    height: 310,
                    color: Colors.white,
                    child: Column(
                      children: <Widget>[
                        const SizedBox(
                          height: 20,
                        ),
                        TextField(
                          controller: headerController,
                          decoration: const InputDecoration(
                              hintText: 'Заголовок',
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.blue, width: 1.0),
                              )),
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        TextField(
                          controller: descriptionController,
                          maxLines: 5,
                          decoration: const InputDecoration(
                              hintText: 'Описание',
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.blue, width: 1.0),
                              )),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ElevatedButton(
                              onPressed: () async {
                                int status = taskTypes.keys.toList().firstWhere(
                                    (element) =>
                                        taskTypes[element] == statusValue);
                                Map<String, dynamic> body = {
                                  "author_login": loginName,
                                  "executor_login": loginName,
                                  "board_name": deskValue,
                                  "task_name": headerController.text,
                                  "status": status,
                                  "task_description": descriptionController.text
                                };
                                boards
                                    .firstWhere(
                                        (element) => element.name == deskValue)
                                    .addNewTask(Task(
                                        0,
                                        headerController.text,
                                        User(0, 'name', 'surname', 'patronomyc',
                                            loginName, 'email'),
                                        User(0, 'name', 'surname', 'patronomyc',
                                            loginName, 'email'),
                                        '',
                                        status,
                                        descriptionController.text));
                                Response resp = await post(
                                    Uri.parse(
                                        'http://127.0.0.1:8000/tasks/createtask/'),
                                    body: json.encode(body));
                                print(resp.body);
                                Navigator.pop(context);
                              },
                              style: const ButtonStyle(
                                  backgroundColor:
                                      MaterialStatePropertyAll<Color>(
                                          Colors.blue)),
                              child: const Text('Создать'),
                            ),
                            ElevatedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              style: const ButtonStyle(
                                  backgroundColor:
                                      MaterialStatePropertyAll<Color>(
                                          Colors.blue)),
                              child: const Text('Отмена'),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    height: 310,
                    color: Colors.grey.shade200,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            const SizedBox(
                              width: 20,
                            ),
                            const Text('Автор'),
                            const SizedBox(
                              width: 50,
                            ),
                            Text(loginName),
                          ],
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        textWithDropdownButton('Доска', boardNames, 'desk'),
                        const SizedBox(
                          width: 20,
                        ),
                        textWithDropdownButton(
                            'Статус',
                            [
                              'Backlog',
                              'ToDo',
                              'InProgress',
                              'Cancel',
                              'Hold',
                              'Done'
                            ],
                            'status')
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          const Expanded(
            flex: 1,
            child: SizedBox(),
          ),
        ],
      ),
    );
  }

  Widget textWithDropdownButton(
      String header, List<String> variants, String type) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        const SizedBox(
          width: 20,
        ),
        Text(header),
        const SizedBox(
          width: 50,
        ),
        DropdownButton(
          items: variants
              .map((e) => DropdownMenuItem(
                    value: e,
                    child: Text(e),
                  ))
              .toList(),
          onChanged: (item) {
            setState(() {
              type == 'desk' ? deskValue = item! : statusValue = item!;
            });
          },
          value: type == 'desk' ? deskValue : statusValue,
        ),
      ],
    );
  }
}
