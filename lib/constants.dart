import 'package:flutter/material.dart';
import 'package:log/models/task.dart';

import 'models/board.dart';

InputBorder border = const OutlineInputBorder(
  borderSide: BorderSide(color: Colors.blue, width: 1.0),
);

String loginName = '';
List<String> boardNames = [];

Map<int, String> taskTypes = {
  0: 'Backlog',
  1: 'ToDo',
  2: 'InProgress',
  3: 'Cancel',
  4: 'Hold',
  5: 'Done'
};

List<Board> boards = [];

List<List<Task>> taskLists = [];